package roadCamera.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DateDTO {

    @JsonProperty("date and time")
    private String date;

    public DateDTO() {
    }

    public DateDTO(String date) {
        this.date = date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
