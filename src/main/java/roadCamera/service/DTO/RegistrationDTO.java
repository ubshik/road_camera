package roadCamera.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

public class RegistrationDTO implements Serializable{

    @JsonProperty("date and time")
    String timestamp;
    @JsonProperty("car number")
    @NotEmpty(message = "Car number mast not be empty.")
    String carNumber;

    public RegistrationDTO() {
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public static RegistrationDTOBuilder builder(){
        return new RegistrationDTOBuilder();
    }

    public static class RegistrationDTOBuilder {
        private RegistrationDTO registrationDTO = new RegistrationDTO();

        public RegistrationDTOBuilder timestamp(String date){
            registrationDTO.timestamp = date;
            return this;
        }

        public RegistrationDTOBuilder carNumber(String carNumber){
            registrationDTO.carNumber = carNumber;
            return this;
        }

        public RegistrationDTO build(){
            return registrationDTO;
        }
    }
}
