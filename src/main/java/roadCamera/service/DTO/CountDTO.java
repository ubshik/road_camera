package roadCamera.service.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountDTO {

    @JsonProperty("Total entries")
    private long count;

    public CountDTO() {
    }

    public CountDTO(long count) {
        this.count = count;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
