package roadCamera.controller;

import org.hibernate.mapping.KeyValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import roadCamera.repository.domain.Registration;
import roadCamera.service.mapping.Mapping;
import roadCamera.service.DTO.RegistrationDTO;
import roadCamera.service.service.RegistrationService;
import roadCamera.validation.CarNumberExistenceConstraint;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping(value = "/registrations",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Validated
public class RegistrationController {
    @Autowired
    private RegistrationService registrationService;
    @Autowired
    private Mapping mapping;

    @PostMapping
    public ResponseEntity saveRegistration(@Valid @RequestBody RegistrationDTO registration){
        registrationService.save(mapping.toObject(registration));
        return new ResponseEntity<>("Registration was added successful".toCharArray(), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity getAllRegistrations(){
        List<Registration> registrations = registrationService.getAll();
        if(registrations.isEmpty()){
            return new ResponseEntity<>(0, HttpStatus.OK);
        }
        return new ResponseEntity<>(mapping.toListDTO(registrations), HttpStatus.OK);
    }

    @GetMapping(value = "/{number}", consumes = MediaType.ALL_VALUE)
    public ResponseEntity getRegistrationsByNumber(@CarNumberExistenceConstraint @PathVariable("number") String carNumber){
        List<Date> dateList = registrationService.getByCarNumber(carNumber);
        return new ResponseEntity<>(mapping.toDateListDTO(dateList), HttpStatus.OK);
    }

    @GetMapping(value = "/stats/count")
    public ResponseEntity getRegistrationCount(){
        long count = registrationService.count();
        return new ResponseEntity<>(mapping.toCountDTO(count), HttpStatus.OK);
    }
}
