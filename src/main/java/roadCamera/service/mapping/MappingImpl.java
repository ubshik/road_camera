package roadCamera.service.mapping;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import roadCamera.repository.domain.Registration;
import roadCamera.service.DTO.CountDTO;
import roadCamera.service.DTO.DateDTO;
import roadCamera.service.DTO.RegistrationDTO;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class MappingImpl implements Mapping {

    @Override
    public Registration toObject(RegistrationDTO from) {
        return Registration.builder().timestamp().carNumber(from.getCarNumber()).build();
    }

    @Override
    public RegistrationDTO toDTO(Registration from) {
        return RegistrationDTO.builder()
                .timestamp(toStringFormat(from.getTimestamp()))
                .carNumber(from.getCarNumber())
                .build();
    }

    @Override
    public List<RegistrationDTO> toListDTO(List<Registration> from) {
        return from.stream().map(this::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<DateDTO> toDateListDTO(List<Date> from) {
        return from.stream().map(this::toDateDTO)
                .collect(Collectors.toList());
    }

    @Override
    public CountDTO toCountDTO(long from) {
        return new CountDTO(from);
    }

    private DateDTO toDateDTO(Date date){
        return new DateDTO(toStringFormat(date));
    }

    private String toStringFormat(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }
}
