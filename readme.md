CREATION DATE 12.02.2018

==================================================================================

**Project essence is Road camera web-service, that has next methods**

-- POST /registrations - add car number as registration (time is server time);

-- GET /registrations - get all registrations from database;

-- GET /registrations/{number} - get all registrations by enter car number;

-- GET /registrations/stats/count - get quantity of all registrations.

==================================================================================

**JSON contains fallow fields:**

-- "date and time" (this field would be received only as ResponseEntity);

-- "car number" (this field is necessary for adding registration).

==================================================================================

**Database is H2, mode is in-memory.**

==================================================================================

Tools are JDK8, Maven.

