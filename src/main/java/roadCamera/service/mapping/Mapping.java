package roadCamera.service.mapping;

import roadCamera.repository.domain.Registration;
import roadCamera.service.DTO.CountDTO;
import roadCamera.service.DTO.DateDTO;
import roadCamera.service.DTO.RegistrationDTO;

import java.util.Date;
import java.util.List;

public interface Mapping {
    Registration toObject(RegistrationDTO from);
    RegistrationDTO toDTO (Registration from);
    List<RegistrationDTO> toListDTO(List<Registration> from);
    List<DateDTO> toDateListDTO(List<Date> from);
    CountDTO toCountDTO(long from);
}
